/**
 * 
 */
package net.duohuo.dhroid.app;

import net.duohuo.dhroid.Const;
import net.duohuo.dhroid.Dhroid;
import net.duohuo.dhroid.adapter.ValueFix;
import net.duohuo.dhroid.db.DhDB;
import net.duohuo.dhroid.dialog.DialogImpl;
import net.duohuo.dhroid.dialog.IDialog;
import net.duohuo.dhroid.ioc.Instance.PerpareAction;
import net.duohuo.dhroid.ioc.Ioc;
import net.duohuo.dhroid.ioc.IocContainer;
import net.duohuo.dhroid.ioc.Instance.InstanceScope;


import android.app.Application;

import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * @author wentaozhong
 *
 */
public class DemoApplication  extends Application {
	@Override
	public void onCreate() {
		// TODO 自动生成的方法存根
		super.onCreate();
		//一些常量的配置
		Const.netadapter_page_no = "p";
		Const.netadapter_step = "step";
		Const.response_data = "data";
		Const.netadapter_step_default = 7;
		Const.netadapter_json_timeline="pubdate";
		Const.DATABASE_VERSION = 20;
		Const.net_pool_size=30;
		Const.net_error_try=true;
		Dhroid.init(this);
	}

}
